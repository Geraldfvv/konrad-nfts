import { Link } from "react-router-dom";

export const AnchorSvg = (props) => {
  const { svg, url, alt } = props;
  const block = "svg-anchor";

  return (
    <Link to={url} className={`${block}__root`}>
      <img className={`${block}__img`} src={svg} alt={alt} />
    </Link>
  );
};
