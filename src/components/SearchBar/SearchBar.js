import "./SearchBar.scss";

export const SearchBar = ({ val, onChange, text }) => {
  const block = "search-bar";
  return (
    <div className={`${block}__root`}>
      <input
        value={val}
        onChange={onChange}
        placeholder={text}
        className={`${block}__input`}
      />
    </div>
  );
};
