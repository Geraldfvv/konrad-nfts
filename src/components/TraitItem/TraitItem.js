export const TraitItem = (props) => {
  const { jkey, value, porcentaje } = props;
  const block = "traits-item";
  return (
    <>
      <div className={`${block}__root`}>
        <span className={`${block}__trait`}>{jkey}</span>
        <span className={`${block}__value`}>{value}</span>
        <span className={`${block}__porcentaje`}>
          {porcentaje} Have this trait
        </span>
      </div>
    </>
  );
};
