export const InfoCard = (props) => {
  const [img, title, text] = props;
  const block = "info-card";

  return (
    <div className={`${block}__root`}>
      <img src={img} alt={title}></img>
      <h1>{title}</h1>
      <p>{text}</p>
    </div>
  );
};
