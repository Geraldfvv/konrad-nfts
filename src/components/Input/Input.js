export const Input = (props) => {
    const { type, label, value, handleFormChange, id } = props;
    const block = "input";
    return (
        <div className={`${block}__input-label`}>
            <label htmlFor={id} className={`${block}__label`}>
                {label}
            </label>
            <input
                id={id}
                className={`${block}__input ${block}__input--100 `}
                type={type}
                placeholder={`${label}...`}
                value={value}
                onChange={(e) => {
                    handleFormChange(e)
                }}
            />
        </div>
    );
};
