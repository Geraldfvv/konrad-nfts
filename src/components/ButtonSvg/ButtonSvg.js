export const ButtonSvg = (props) => {
  const { svg, handleClick, alt } = props;
  const block = "svg-button";

  return (
    <button
      className={`${block}__root`}
      onClick={() => {
        handleClick();
      }}
    >
      <img className={`${block}__img`} src={svg} alt={alt} />
    </button>
  );
};
