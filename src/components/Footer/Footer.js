import { Logo } from "../Logo/Logo";
import { AnchorSvg } from "../AnchorSvg/AnchorSvg";

import facebook from "../../assets/svgs/facebook.svg";
import instagram from "../../assets/svgs/instagram.svg";
import twitter from "../../assets/svgs/twitter.svg";

import { useLocation } from "react-router-dom";

export const Footer = () => {
  const block = "footer";

  const location = useLocation();

  return (
    <>
      {location.pathname !== "/cart" && location.pathname !== "/checkout" && (
        <>
          <div className={`${block}__root`}>
            <Logo></Logo>
            <div className={`${block}__buttons`}>
              <AnchorSvg svg={facebook} alt='windows' url='/' />
              <AnchorSvg svg={instagram} alt='android' url='/' />
              <AnchorSvg svg={twitter} alt='apple' url='/' />
            </div>
          </div>
          <div className={`${block}__links`}>
            <span>© 2022 Xtraordinary</span>
            <span>Privacy Policy</span>
            <span>Terms of use</span>
          </div>
        </>
      )}
    </>
  );
};
