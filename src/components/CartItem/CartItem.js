import { useDispatch } from "react-redux";
import { deleteItem, updateItem } from "../../reducers/CartReducer";

import trash from "../../assets/svgs/trash.svg";
import etherum from "../../assets/svgs/etherum.svg";

import { ButtonSvg } from "../ButtonSvg/ButtonSvg";
import { useState } from "react";

export const CartItem = (props) => {
  const dispatch = useDispatch();

  const block = "cart-item";
  const { id, name, image, price, collection, quantity, isCart } = props;
  const [qty, setQty] = useState(quantity);

  const handleDeleteItem = () => {
    dispatch(
      deleteItem({
        id: id,
        quantity: quantity,
      })
    );
  };

  const handleInputChange = (event) => {
    if (event.target.value !== "") {
      setQty(event.target.value);
      dispatch(
        updateItem({
          id: id,
          quantity: event.target.value,
        })
      );
    }
  };

  return (
    <>
      <div className={`${block}__root`}>
        <div className={`${block}__nft-container`}>
          <img src={image} className={`${block}__image`} alt={name} />
          <div className={`${block}__info-container`}>
            <span className={`${block}__sub-text`}>{collection}</span>
            <span className={`${block}__main-text`}>{name}</span>
            <span className={`${block}__sub-text`}>
              {isCart && (
                <input
                  className={`${block}__quantity`}
                  value={qty}
                  type='number'
                  onChange={handleInputChange}
                  min='1'
                />
              )}
              {!isCart && <span>{quantity}</span>} x ETH {price}
            </span>
          </div>
        </div>

        <div className={`${block}__subtotal-container`}>
          <span className={`${block}__main-text`}>
            <img
              className={`${block}__eth-logo`}
              alt='Etherum logo'
              src={etherum}
            />
            {(price * quantity)
              .toFixed(3)
              .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
          </span>
          <span className={`${block}__sub-text`}>
            ($
            {(price * quantity * 1681.59)
              .toFixed(3)
              .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
            ){" "}
          </span>
          {isCart && (
            <ButtonSvg
              svg={trash}
              alt={"Delete from cart"}
              handleClick={handleDeleteItem}
            />
          )}
        </div>
      </div>
    </>
  );
};
