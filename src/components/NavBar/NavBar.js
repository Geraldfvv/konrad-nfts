import cart from "../../assets/svgs/cart.svg";
import user from "../../assets/svgs/user.svg";

import { Logo } from "../Logo/Logo";
import { AnchorSvg } from "../AnchorSvg/AnchorSvg";
import { useSelector } from "react-redux";

export const NavBar = () => {
  const block = "nav-bar";
  const cartItems = useSelector((state) => state.cart);

  return (
    <>
      <div className={`${block}__root`}>
        <Logo></Logo>

        <div className={`${block}__cart-container`}>
          <AnchorSvg svg={cart} url={"/cart"} alt='Cart'></AnchorSvg>

          <p className={`${block}__count`}>{parseInt(cartItems.cartLength)}</p>
        </div>

        <AnchorSvg svg={user} url={"/signup"} alt='Cart'></AnchorSvg>

      </div>
    </>
  );
};
