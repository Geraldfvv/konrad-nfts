import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";

import { addItem } from "../../reducers/CartReducer";

import { ButtonSvg } from "../ButtonSvg/ButtonSvg";
import addCart from "../../assets/svgs/add-cart.svg";

export const NftItem = (props) => {
  const dispatch = useDispatch();
  const block = "nft-item";
  const { id, name, description, image, price, collection } = props;

  const handleAddItem = () => {
    dispatch(
      addItem({
        nft: props,
        quantity: 1,
      })
    );
  };

  return (
    <>
      <div className={`${block}__root`}>
        <div className={`${block}__card`}>
          <Link
            to={`/collection/${id}`}
            state={{ ...props }}
            className={`${block}__link`}
          >
            <div className={`${block}__clickeable`}>
              <img className={`${block}__img`} src={image} alt={name} />
              <p className={`${block}__text-title`}>{name}</p>
              <p className={`${block}__text-subtitle`}>{collection}</p>
              <p className={`${block}__text-body`}>{description}</p>
            </div>
          </Link>
          <div className={`${block}__card-footer`}>
            <span className={`${block}__text-title`}>ETH {price}</span>
            <div className={`${block}__card-button`}>
              <ButtonSvg
                svg={addCart}
                alt={"Add to cart"}
                handleClick={handleAddItem}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
