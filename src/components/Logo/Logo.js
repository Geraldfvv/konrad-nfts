import { Link } from "react-router-dom";

export const Logo = () => {
  const block = "logo";
  return (
    <Link to='/' className={`${block}__root`}>
      <img
        className={`${block}__img`}
        alt='Company logo'
        src='https://res.cloudinary.com/crunchbase-production/image/upload/c_lpad,f_auto,q_auto:eco,dpr_1/vegu0zjnxtqaoz8lc5wy'
      />
      <h1 className={`${block}__name`}>traordinary</h1>
    </Link>
  );
};
