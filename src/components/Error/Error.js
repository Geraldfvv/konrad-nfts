export const Error = (props) => {
  const {msg1, msg2} = props;
  const block = "error";
  return (
    <div className={`${block}__root`}>
      <div className={`${block}__icon`}>
        <img
          src='https://cdn.rawgit.com/ahmedhosna95/upload/1731955f/sad404.svg'
          alt='Error fetching data'
        ></img>
        <h1 className={`${block}__text`}>
          {msg1}
          <br /> 
          {msg2}
        </h1>
      </div>
    </div>
  );
};
