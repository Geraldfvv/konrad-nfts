import { Input } from "../../Input/Input";

export const PersonalInfo = ({ formData, handleFormChange }) => {
    const block = "personal-info";
    return (
        <div className={`${block}__root`}>
            <form className={`${block}__form`}>
                <Input
                    type="text"
                    label="First Name"
                    value={formData.firstName}
                    handleFormChange={handleFormChange}
                    id="firstName"
                ></Input>

                <Input
                    type="text"
                    label="Last Name"
                    value={formData.lastName}
                    handleFormChange={handleFormChange}
                    id="lastName"
                ></Input>

                <Input
                    type="text"
                    label="Birthday"
                    value={formData.birthday}
                    handleFormChange={handleFormChange}
                    id="birthday"
                ></Input>

                <div className={`${block}__input-label`}>
                    <label htmlFor="gender" className={`${block}__label`}>
                        Gender
                    </label>
                    <div className={`${block}__input ${block}__input--100`}>
                        <input type="radio" value="Male" name="gender" /> Male
                        <input type="radio" value="Female" name="gender" /> Female
                        <input type="radio" value="Other" name="gender" /> Other
                    </div>
                </div>
            </form>
        </div>
    );
};

export default PersonalInfo;

