import { Input } from "../../Input/Input";

export const AccountInfo = ({ formData, handleFormChange }) => {
    const block = "personal-info";
    return (
        <div className={`${block}__root`}>
            <form className={`${block}__form`}>
                <Input
                    type="text"
                    label="Username"
                    value={formData.username}
                    handleFormChange={handleFormChange}
                    id="username"
                ></Input>

                <Input
                    type="email"
                    label="Email"
                    value={formData.email}
                    handleFormChange={handleFormChange}
                    id="email"
                ></Input>

                <Input
                    type="password"
                    label="Password"
                    value={formData.password}
                    handleFormChange={handleFormChange}
                    id="password"
                ></Input>

                <Input
                    type="password"
                    label="Confirm Password"
                    value={formData.passwordVerification}
                    handleFormChange={handleFormChange}
                    id="passwordVerification"
                ></Input>

            </form>
        </div>
    );
};
