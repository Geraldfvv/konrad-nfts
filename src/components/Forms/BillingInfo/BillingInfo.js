export const BillingInfo = ({ formData, setFormData }) => {
    const block = "billing-info";
    return (
        <div className={`${block}__root`}>
            <form className={`${block}__form`}>
                <div className={`${block}__input-label`}>
                    <label htmlFor="cardType" className={`${block}__label`}>
                        Card type
                    </label>
                    <div
                        className={`${block}__input ${block}__input--100`}
                        id="cardType"
                    >
                        <input
                            type="radio"
                            value="Master Card"
                            name="cardType"
                        />
                        Master Card
                        <input type="radio" value="Visa" name="cardType" /> Visa
                        <input
                            type="radio"
                            value="Other"
                            name="cardType"
                        />
                        Other
                    </div>
                </div>

                <div className={`${block}__input-label`}>
                    <label htmlFor="cardNumber" className={`${block}__label`}>
                        Card number
                    </label>
                    <input
                        id="cardNumber"
                        className={`${block}__input ${block}__input--100`}
                        type="number"
                        placeholder="Card number..."
                        value={formData.cardNumber}
                        onChange={(e) => {
                            setFormData({
                                ...formData,
                                cardNumber: e.target.value,
                            });
                        }}
                    />
                </div>

                <div className={`${block}__input-label`}>
                    <label
                        htmlFor="cardExpiration"
                        className={`${block}__label`}
                    >
                        Expiration Date
                    </label>
                    <input
                        id="cardExpiration"
                        className={`${block}__input ${block}__input--100`}
                        type="month"
                        value={formData.cardExpiration}
                        onChange={(e) => {
                            setFormData({
                                ...formData,
                                cardExpiration: e.target.value,
                            });
                        }}
                    />
                </div>

                <div className={`${block}__input-label`}>
                    <label htmlFor="cardCVS" className={`${block}__label`}>
                        CVS Code
                    </label>
                    <input
                        id="cardCVS"
                        className={`${block}__input ${block}__input--100`}
                        type="number"
                        placeholder="CVS..."
                        value={formData.cardCVS}
                        onChange={(e) => {
                            setFormData({
                                ...formData,
                                cardCVS: e.target.value,
                            });
                        }}
                    />
                </div>
            </form>
        </div>
    );
};
