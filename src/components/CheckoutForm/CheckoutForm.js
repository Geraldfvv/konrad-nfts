import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

import { proceedPayment, purchasConfirmation } from "../../alerts/alerts";

import { emptyCart } from "../../reducers/CartReducer";
import { Button } from "../Button/Button";

export const CheckoutForm = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const block = "checkout-form";

    const [formValue, setFormValue] = useState({
        firstName: "",
        LastName: "",
        email: "",
        country: "",
        state: "",
        city: "",
        address: "",
        zip: "",
        cardName: "",
        cardNumber: "",
        expDate: "",
        cvc: "",
        wallet: "",
    });

    function handleFormChange(event) {
        const value = event.target.value;
        setFormValue({ ...formValue, [event.target.name]: value });
    }

    const handleSubmit = () => {
        proceedPayment().then(() => {
            dispatch(emptyCart());
            navigate("/");
            purchasConfirmation();
        });
    };

    return (
        <>
            <div className={`${block}__root`}>
                <form className={`${block}__form`}>
                    <span className={`${block}__title`}>Costumer Info</span>

                    <input
                        id="firstName"
                        className={`${block}__input ${block}__input--33`}
                        placeholder="First Name *"
                        type="text"
                        name="firstName"
                        value={formValue.firstName}
                        onChange={handleFormChange}
                        required
                    />

                    <input
                        className={`${block}__input ${block}__input--33`}
                        placeholder="Last Name *"
                        type="text"
                        name="lastName"
                        value={formValue.lastName}
                        onChange={handleFormChange}
                        required
                    />

                    <input
                        className={`${block}__input ${block}__input--33`}
                        placeholder="Email *"
                        type="email"
                        name="email"
                        value={formValue.email}
                        onChange={handleFormChange}
                        required
                    />

                    <input
                        className={`${block}__input ${block}__input--33`}
                        placeholder="Country *"
                        type="text"
                        name="country"
                        value={formValue.country}
                        onChange={handleFormChange}
                        required
                    />

                    <input
                        className={`${block}__input ${block}__input--33`}
                        placeholder="State *"
                        type="text"
                        name="state"
                        value={formValue.state}
                        onChange={handleFormChange}
                        required
                    />

                    <input
                        className={`${block}__input ${block}__input--33`}
                        placeholder="City *"
                        type="text"
                        name="city"
                        value={formValue.city}
                        onChange={handleFormChange}
                        required
                    />

                    <textarea
                        rows={3}
                        className={`${block}__input ${block}__input--100`}
                        placeholder="Address *"
                        type="text-area"
                        name="address"
                        value={formValue.address}
                        onChange={handleFormChange}
                        required
                    />

                    <span className={`${block}__title`}>Payment Info</span>

                    <input
                        className={`${block}__input ${block}__input--25`}
                        placeholder="Card Holder Name *"
                        type="text"
                        name="cardName"
                        value={formValue.cardName}
                        onChange={handleFormChange}
                        required
                    />

                    <input
                        className={`${block}__input ${block}__input--25`}
                        placeholder="Card Number *"
                        type="number"
                        maxLength="16"
                        name="cardNumber"
                        value={formValue.cardNumber}
                        onChange={handleFormChange}
                        required
                    />

                    <input
                        className={`${block}__input ${block}__input--25`}
                        placeholder="Expiration Date *"
                        type="month"
                        name="expDate"
                        value={formValue.expDate}
                        onChange={handleFormChange}
                        required
                    />

                    <input
                        className={`${block}__input ${block}__input--25`}
                        placeholder="CVC *"
                        type="number"
                        name="cvc"
                        value={formValue.cvc}
                        onChange={handleFormChange}
                        required
                    />

                    <span className={`${block}__title`}>Wallet</span>

                    <input
                        className={`${block}__input ${block}__input--50`}
                        placeholder="Wallet ID *"
                        type="text"
                        name="wallet"
                        value={formValue.wallet}
                        onChange={handleFormChange}
                        required
                    />

                    <div className={`${block}__button-container`}>
                        <Button
                            type="submit"
                            theme="secondary-background"
                            text="Proceed"
                            handleClick={handleSubmit}
                        ></Button>
                    </div>
                </form>
            </div>
        </>
    );
};
