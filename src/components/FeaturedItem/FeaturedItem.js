export const FeaturedItem = (props) => {
  const { url, collection, author, authorImg } = props;
  return (
    <div className='featured__root'>
      <img className='featured__image' src={url} alt='Featured Collection' />
      <div className='featured__info'>
        <img
          className='featured__info__porfile'
          src={authorImg}
          alt='profile pic'
        />
        <div>
          <p className='featured__info__title'>{collection}</p>
          <p className='featured__info__author'>
            Featured collection by {author}
          </p>
        </div>
      </div>
    </div>
  );
};
