import { Outlet } from "react-router-dom";
import { Provider } from "react-redux";
import store from "../src/app/store";
import "./App.scss";

import { NavBar } from "./components/NavBar/NavBar";
import { Footer } from "./components/Footer/Footer"

function App() {
  return (
    <>
      <Provider store={store}>
        <header>
          <NavBar />
        </header>

        <main >
          <Outlet />
        </main>

        <footer>
          <Footer/>
        </footer>
      </Provider>
    </>
  );
}

export default App;
