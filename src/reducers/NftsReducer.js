import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

const initialState = {
  loading: false,
  nfts: [],
  error: "",
};

export const fetchNfts = createAsyncThunk("nfts/fetchNfts", (url) => {
  return fetch(url)
    .then((response) => response.json())
    .then((data) => data);
});

export const nftSlice = createSlice({
  name: "nfts",
  initialState,
  extraReducers: (builder) => {
    
    builder.addCase(fetchNfts.pending, (state) => {
      state.loading = true;
    });

    builder.addCase(fetchNfts.fulfilled, (state, action) => {
      state.loading = false;
      state.nfts = action.payload;
      state.error = "";
    });

    builder.addCase(fetchNfts.rejected, (state, action) => {
      state.loading = false;
      state.nfts = [];
      state.error = action.error.message;
    });
  },
});

export default nftSlice.reducer;
