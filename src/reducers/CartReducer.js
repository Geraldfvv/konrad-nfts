import { createSlice } from "@reduxjs/toolkit";
import { showMessage } from "../alerts/alerts";

const saveLocalStorage = (state) => {
  localStorage.setItem("cart", JSON.stringify(state));
};

const getLocalStorage = (key) => {
  let data = JSON.parse(localStorage.getItem("cart"));
  if (data !== null) {
    if (key === "value") {
      return data.value;
    } else {
      return data.cartLength;
    }
  }
  if (key === "value") {
    return [];
  } else {
    return 0;
  }
};

const initialState = {
  value: getLocalStorage("value"),
  cartLength: getLocalStorage("cartLength"),
};

export const cartSlice = createSlice({
  name: "cart",
  initialState: initialState,
  reducers: {
    addItem: (state, action) => {
      let item = state.value.filter(
        (element) => element.nft.id === action.payload.nft.id
      );

      if (item.length !== 0) {
        let itemIndex = state.value.indexOf(item[0]);
        state.value[itemIndex].quantity += action.payload.quantity;
        state.cartLength += action.payload.quantity;
      } else {
        state.value.push(action.payload);
        state.cartLength += action.payload.quantity;
      }
      saveLocalStorage(state);

      showMessage(`${action.payload.nft.name} added to cart`, "success");
    },

    deleteItem: (state, action) => {
      state.value = state.value.filter(
        (element) => element.nft.id !== action.payload.id
      );
      state.cartLength -= action.payload.quantity;
      saveLocalStorage(state);
    },

    updateItem: (state, action) => {
      state.value.map((element) => {
        if (element.nft.id === action.payload.id) {
          state.cartLength -= parseInt(element.quantity);
          state.cartLength += parseInt(action.payload.quantity);
          element.quantity = parseInt(action.payload.quantity);
          saveLocalStorage(state);
        }
        return null;
      });
    },

    emptyCart: (state, action) => {
      state.value = [];
      state.cartLength = 0;
      saveLocalStorage(state);
    },
  },
});

export const { addItem, deleteItem, emptyCart, updateItem } = cartSlice.actions;
export default cartSlice.reducer;
