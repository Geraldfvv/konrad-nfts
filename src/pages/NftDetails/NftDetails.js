import { useLocation } from "react-router-dom";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";

import { addItem } from "../../reducers/CartReducer";
import { Button } from "../../components/Button/Button";
import { TraitItem } from "../../components/TraitItem/TraitItem";

import etherum from "../../assets/svgs/etherum.svg";

export const NftDetails = () => {
  const block = "item";
  const nft = useLocation().state;
  const dispatch = useDispatch();
  const { name, description, image, price, collection, createdBy, traits } =
    nft;

  const handleAddItem = () => {
    dispatch(
      addItem({
        nft: nft,
        quantity: 1,
      })
    );
  };

  return (
    <>
      <div className={`${block}__root`}>
        <div className={`${block}__image-container`}>
          <img className={`${block}__image`} src={image} alt={name} />
        </div>

        <div className={`${block}__info-container`}>
          <div className={`${block}__title-container `}>
            <div>
              <h1 className={`${block}__title`}>{name}</h1>
              <Link to='/users' className={`${block}__link`}>
                {collection} by {createdBy}
              </Link>
            </div>
          </div>

          <p className={`${block}__description`}>{description}</p>

          <h1 className={`${block}__title`}>Traits</h1>
          <div className={`${block}__properties-container`}>
            {traits.map((trait) => {
              const [jkey, value] = Object.entries(trait)[0];
              return (
                <TraitItem
                  key={jkey}
                  jkey={jkey}
                  value={value}
                  porcentaje={trait.porcentaje}
                />
              );
            })}
          </div>

          <div className={`${block}__price-container`}>
            <img
              className={`${block}__eth-logo`}
              alt='Etherum logo'
              src={etherum}
            />
            <span className={`${block}__price`}>{price}</span>
            <span className={`${block}__price--usd`}>
              ($
              {(price * 1681.59)
                .toFixed(3)
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
              )
            </span>
          </div>

          <div className={`${block}__button-container`}>
            <Button
              theme={"secondary-background"}
              text={"Add to cart"}
              handleClick={handleAddItem}
            ></Button>
          </div>
        </div>
      </div>
    </>
  );
};
