import { AnchorBtn } from "../../components/AnchorBtn/AnchorBtn";
import { FeaturedItem } from "../../components/FeaturedItem/FeaturedItem";
import { AnchorSvg } from "../../components/AnchorSvg/AnchorSvg";

import android from "../../assets/svgs/android.svg";
import apple from "../../assets/svgs/apple.svg";
import huawei from "../../assets/svgs/huawei.svg";
import windows from "../../assets/svgs/windows.svg";

export const Home = () => {
    const infoBlock = "info-block";

    return (
        <>
            <section>
                <div className="hero__root">
                    <div className="hero__background"></div>
                    <div className="hero__info">
                        <span className="hero__title">
                            Discover, collect, and sell Xtraordinary NFTs
                        </span>
                        <span className="hero__sub-title">
                            The NFT marketplace with everything for everyone
                        </span>
                        <div className="hero__buttons">
                            <AnchorBtn
                                className="hero__button"
                                text={"Explore"}
                                theme={"primary-background"}
                                url={"/collection"}
                            />
                            <AnchorBtn
                                className="hero__button"
                                text={"Learn More"}
                                theme={"primary-background"}
                                url={"/collection"}
                            />
                        </div>
                    </div>
                    <FeaturedItem
                        url={
                            "https://lh3.googleusercontent.com/xaq2F0_OcTuAP5f_XDbIxsXvBo8XKcLXO5gAyhc1n_RCzCoB9muLMeGDluM6EazCx0c-_ZajBWTWWPBu7AC_IDfgHyc1qfpCP7y4tGw=w600"
                        }
                        authorImg={
                            "https://lh3.googleusercontent.com/qQvvJI0kbm-PRRwSWbsreHpiXUfVIKddivqXb0lLPasCcSOQgBscS5_MiNIhn9_akfCwRLbznPjkEFqjsSH2xM8EmEiy0b_dw4pp=w600"
                        }
                        author={"BgbNoh"}
                        collection={"META HUNTERS"}
                    />
                </div>
            </section>

            <section className={`${infoBlock}__root`}>
                <h1 className={`${infoBlock}__title`}>Why Xtraordinary?</h1>
                <div className={`${infoBlock}__info-card`}>
                    <img
                        className={`${infoBlock}__icon`}
                        src="https://opensea.io/static/images/icons/wallet.svg"
                        alt="wallet"
                    />
                    <h2>$76 billion</h2>
                    <span>Trading volume exchange</span>
                </div>
                <div className={`${infoBlock}__info-card`}>
                    <img
                        className={`${infoBlock}__icon`}
                        src="https://static.opensea.io/about/icon-users.svg"
                        alt="users"
                    />
                    <h2>90 million</h2>{" "}
                    <span>Registered users who trust Xtraordinary</span>
                </div>
                <div className={`${infoBlock}__info-card`}>
                    <img
                        className={`${infoBlock}__icon`}
                        src="https://opensea.io/static/images/icons/sale.svg"
                        alt="Offer tags"
                    />
                    <h2>{"<0.10% "}</h2>
                    <span>Lowest transaction fees</span>
                </div>
                <div className={`${infoBlock}__info-card`}>
                    <img
                        className={`${infoBlock}__icon`}
                        src="https://opensea.io/static/images/icons/collection.svg"
                        alt="collection"
                    />
                    <h2>2 million</h2>
                    <span>Collections of all kind</span>
                </div>
            </section>

            <section className={`${infoBlock}__root`}>
                <h1 className={`${infoBlock}__title`}>
                    Trade on the go. Anywhere, anytime.
                </h1>

                <div className="container--100">
                    <div className="container--100">
                        <AnchorSvg
                            svg={windows}
                            alt="windows"
                            url="/"
                        ></AnchorSvg>
                        <AnchorSvg
                            svg={android}
                            alt="android"
                            url="/"
                        ></AnchorSvg>
                        <AnchorSvg svg={apple} alt="apple" url="/"></AnchorSvg>
                        <AnchorSvg
                            svg={huawei}
                            alt="huawei"
                            url="/"
                        ></AnchorSvg>
                    </div>
                    <img
                        src="https://images.ctfassets.net/9sy2a0egs6zh/78HoDbPwuWz8M6er6joJdE/c440f3e5d7262a424f13da69a46e958a/wallet-illo.svg"
                        alt="Cellphone and money"
                        className={`${infoBlock}__img`}
                    ></img>
                </div>
            </section>

            <section className={`${infoBlock}__root`}>
                <h1 className={`${infoBlock}__title`}>Need Help?</h1>
                <div className={`${infoBlock}__info-card`}>
                    <img
                        className={`${infoBlock}__icon`}
                        src="https://static.opensea.io/about/icon-timeline.svg"
                        alt="wallet"
                    />
                    <h2>24/7 Chat Support</h2>
                    <span>
                        Get 24/7 chat support with our friendly customer service
                        agents at your service.
                    </span>
                </div>
                <div className={`${infoBlock}__info-card`}>
                    <img
                        className={`${infoBlock}__icon`}
                        src="https://static.opensea.io/about/nfts.svg"
                        alt="users"
                    />
                    <h2>FAQs</h2>{" "}
                    <span>
                        View FAQs for detailed instructions on specific
                        features.
                    </span>
                </div>
                <div className={`${infoBlock}__info-card`}>
                    <img
                        className={`${infoBlock}__icon`}
                        src="https://static.opensea.io/about/icon-money.svg"
                        alt="training"
                    />
                    <h2>NFT Training</h2>
                    <span>
                        Xtraordinary gives you multiple options when it comes to
                        your passive income.
                    </span>
                </div>
            </section>
        </>
    );
};
