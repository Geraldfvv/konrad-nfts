import { useState } from "react";
import { Button } from "../../components/Button/Button";
import { AccountInfo } from "../../components/Forms/AccountInfo/AccountInfo";
import { BillingInfo } from "../../components/Forms/BillingInfo/BillingInfo";
import PersonalInfo from "../../components/Forms/PersonalInfo/PersonalInfo";

export const SignUp = () => {
    const block = "sign-up";
    const [page, setPage] = useState(0);
    const [formData, setFormData] = useState({
        firstName: "",
        lastName: "",
        birthday: "",
        gender: "",
        username: "",
        email: "",
        password: "",
        passwordVerification: "",
        cardType: "",
        cardNumber: "",
        cardExpiration: "",
        cardCVS: "",
    });

    const formTitles = [
        "Personal Information",
        "Account Information",
        "Billing Information",
    ];

    const pageDisplay = () => {
        if (page === 0) {
            return (
                <PersonalInfo formData={formData}  handleFormChange={handleFormChange} />
            );
        } else if (page === 1) {
            return (
                <AccountInfo formData={formData} setFormData={setFormData} handleFormChange={handleFormChange}/>
            );
        } else {
            return (
                <BillingInfo formData={formData} setFormData={setFormData} handleFormChange={handleFormChange} />
            );
        }
    };

    const handlePrevious = () => {
        setPage((currPage) => currPage - 1);
    };

    const handleNext = () => {
        if (page === formTitles.length - 1) {
            alert("FORM SUBMITTED");
            console.log(formData)
        } else {
            setPage((currPage) => currPage + 1);
        }
    };

    const handleFormChange = (event) => {
        const value = event.target.value;
        setFormData({ ...formData, [event.target.id]: value });
    };

    return (
        <div className={`${block}__root`}>
            <div className={`${block}__header`}>
                <div className={`${block}__stepper`}>
                    {formTitles.map((step, i) => {
                        return (
                            <span
                                aria-label={`step ${i + 1}`}
                                className={`${block}__step ${
                                    i === page ? `${block}__step--active` : ""
                                }`}
                            >
                                {i + 1}
                            </span>
                        );
                    })}
                </div>

                <h1 className={`${block}__title`}>{formTitles[page]}</h1>
            </div>

            <div className={`${block}__body`}>{pageDisplay()}</div>

            <div className={`${block}__footer`}>
                <Button
                    theme={"secondary-background"}
                    text={"Previous"}
                    disabled={page === 0}
                    handleClick={handlePrevious}
                ></Button>

                <Button
                    theme={"secondary-background"}
                    text={page === formTitles.length - 1 ? "Submit" : "Next"}
                    handleClick={handleNext}
                ></Button>
            </div>
        </div>
    );
};
