import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchNfts } from "../../reducers/NftsReducer";
import { Loading } from "../../components/Loading/Loading";
import { Error } from "../../components/Error/Error";
import { NftItem } from "../../components/NftItem/NftItem";
import { SearchBar } from "../../components/SearchBar/SearchBar";

export const NftList = () => {
  const block = "nft-list";
  const nfts = useSelector((state) => state.nft);
  const [searchWord, setSearchWord] = useState("");
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchNfts("http://localhost:4000/nfts"));
  }, [dispatch]);

  const handleInputChange = (event) => {
    setSearchWord(event.target.value);
  };

  return (
    <div>
      {nfts.loading && <Loading />}
      {!nfts.loading && nfts.error ? <Error msg1={"Error loading data,"} msg2={"please try again."} /> : null}
      {!nfts.loading && !nfts.error && (
        <>
          <div className={`${block}__nft-container`}>
            <SearchBar
              onChange={handleInputChange}
              val={searchWord}
              text={"Search by collection..."}
            />
            {nfts.nfts
              .filter((nft) =>
                nft.collection.toLowerCase().includes(searchWord.toLowerCase())
              )
              .map((nft) => {
                return <NftItem key={nft.id} {...nft}></NftItem>;
              })}
          </div>
        </>
      )}
    </div>
  );
};
