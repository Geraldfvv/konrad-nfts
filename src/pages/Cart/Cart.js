import { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import { CartItem } from "../../components/CartItem/CartItem";
import { AnchorBtn } from "../../components/AnchorBtn/AnchorBtn";
import { Error } from "../../components/Error/Error";
import etherum from "../../assets/svgs/etherum.svg";

export const Cart = () => {
  const block = "cart";
  const cartItems = useSelector((state) => state.cart);
  const [total, setTotal] = useState(0);

  useEffect(() => {
    let newtotal = 0;
    cartItems.value.forEach((item) => {
      newtotal += parseFloat(item.nft.price) * item.quantity;
    });
    setTotal(newtotal);
  }, [cartItems]);

  return (
    <>
      {cartItems.value.length === 0 && (
        <Error msg1={"You have no items "} msg2={"in your shopping cart."} />
      )}

      <div className={`${block}__root`}>
        <div className={`${block}__item-container`}>
          {cartItems.value.map((item) => {
            return (
              <CartItem
                key={item.nft.id}
                {...item.nft}
                quantity={item.quantity}
                isCart={true}
              />
            );
          })}
        </div>

        <div className={`${block}__to-checkout`}>
          <span className={`${block}__title`}>
            Total:{" "}
            <img
              className={`${block}__eth-logo`}
              alt='Etherum logo'
              src={etherum}
            />{" "}
            {total.toFixed(3).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
          </span>
          {cartItems.cartLength !== 0 && (
            <AnchorBtn
              text={"Checkout"}
              theme={"secondary-background"}
              url={"/checkout"}
            />
          )}
        </div>
      </div>
    </>
  );
};
