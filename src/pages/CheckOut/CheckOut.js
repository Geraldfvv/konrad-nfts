import { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import etherum from "../../assets/svgs/etherum.svg";

import { CheckoutForm } from "../../components/CheckoutForm/CheckoutForm";
import { CartItem } from "../../components/CartItem/CartItem";

export const CheckOut = () => {
  const block = "checkout";
  const cartItems = useSelector((state) => state.cart);
  const [totalEth, setTotalEth] = useState(0);
  const [totalUsd, setTotalUsd] = useState(0);

  useEffect(() => {
    let newtotal = 0;
    const regex = new RegExp(/\B(?=(\d{3})+(?!\d))/g);

    cartItems.value.forEach((item) => {
      newtotal += parseFloat(item.nft.price) * item.quantity;
    });
    setTotalEth(newtotal.toFixed(3).replace(regex, ","));
    setTotalUsd((newtotal * 1681.59).toFixed(3).replace(regex, ","));
  }, [cartItems]);

  return (
    <div className={`${block}__root`}>
      <div className={`${block}__cart-container`}>
        <span className={`${block}__title`}>Your Order</span>
        {cartItems.value.map((item) => {
          return (
            <CartItem
              key={item.nft.id}
              {...item.nft}
              quantity={item.quantity}
              isCart={false}
            />
          );
        })}

        <div className={`${block}__total`}>
          <div className={`${block}__eth`}>
            <img
              className={`${block}__eth-logo`}
              alt='Etherum logo'
              src={etherum}
            />
            <span className={`${block}__price`}> {totalEth}</span>
          </div>
          <span className={`${block}__price--usd`}>(${totalUsd})</span>
        </div>
      </div>

      <div className={`${block}__forms-container`}>
        <CheckoutForm></CheckoutForm>
      </div>
    </div>
  );
};
