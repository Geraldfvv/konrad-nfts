import { configureStore } from "@reduxjs/toolkit";
import nftReducer from "../reducers/NftsReducer";
import cartReducer from "../reducers/CartReducer";

const store = configureStore({
  reducer: {
    nft: nftReducer,
    cart: cartReducer
  },
});

export default store;
