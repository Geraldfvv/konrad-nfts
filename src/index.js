import "./index.scss";
import "./styles/styles.scss";

import React from "react";
import ReactDOM from "react-dom/client";

import reportWebVitals from "./reportWebVitals";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import App from "./App";
import { Cart } from "./pages/Cart/Cart";
import { CheckOut } from "./pages/CheckOut/CheckOut";
import { Home } from "./pages/Home/Home";
import { NftDetails } from "./pages/NftDetails/NftDetails";
import { NftList } from "./pages/NftList/NftList";
import { SignUp } from "./pages/SignUp/SignUp";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<App />}>
          <Route path='/' element={<Home />} />
          <Route path='collection' element={<NftList />} />
          <Route path='collection/:id' element={<NftDetails />} />
          <Route path='cart' element={<Cart />} />
          <Route path='checkout' element={<CheckOut />} />
          <Route path='signup' element={<SignUp />} />
        </Route>
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
